using System.ComponentModel.DataAnnotations;

namespace mi_tiendita_backend.Models;

public class User
{
    [Key]
    public int userId { get; set; }
    [Required]
    [MaxLength(100)]
    public string name { get; set; }
    [Required]
    public string lastname { get; set; }
    [Required]
    public string username { get; set; }
    [Required]
    public string password { get; set; }
    public string avatarUrl { get; set; }
    [Required]
    public DateTime createdAt { get; set; }
    [Required]
    public DateTime updatedAt { get; set; }
    [Required]
    public bool deleted { get; set; }
}
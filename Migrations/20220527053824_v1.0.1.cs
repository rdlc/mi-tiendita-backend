﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace mi_tiendita_backend.Migrations
{
    /// <inheritdoc />
    public partial class v101 : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_MiTiendita",
                table: "MiTiendita");

            migrationBuilder.RenameTable(
                name: "MiTiendita",
                newName: "User");

            migrationBuilder.AddPrimaryKey(
                name: "PK_User",
                table: "User",
                column: "userId");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_User",
                table: "User");

            migrationBuilder.RenameTable(
                name: "User",
                newName: "MiTiendita");

            migrationBuilder.AddPrimaryKey(
                name: "PK_MiTiendita",
                table: "MiTiendita",
                column: "userId");
        }
    }
}

using Microsoft.EntityFrameworkCore;
using mi_tiendita_backend.Models;

namespace mi_tiendita_backend;

public class ApplicationDbContext : DbContext
{
    public DbSet<User> User { get; set; }

    public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
    {

    }
}